<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $token = request()->bearerToken();
        // return response()->json(compact('token'));
        $customers = Customer::all();
        return response()->json($customers);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $customer = request()->all();
        
        // $id = DB::table('customers')->insertGetId($customer);

        return response()->json(['Hello'=>'World']); // ['id' => $id]
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = DB::table('customers')->where('id',$id)->first();
        if (isset($customer)) {
            return response()->json($customer);
        }else{
            return response([],404);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $customer = request()->except('token');
        $rows =  DB::table('customers')->where('id',$id)->update($customer);
        return response()->json(compact('rows'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $success = DB::table('customers')->where('id',$id)->delete();
       return response()->json(compact('success'));

    }
}
