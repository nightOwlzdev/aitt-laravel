<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    public function login(){
        $input = request()->only('email','password');

        $key = 'SECRETAITT';

        if (auth()->validate($input)) {

            $payload = [
                'email' => $input['email']
            ];

            $token = JWT::encode($payload,$key);
            return response()->json(compact('token'));
        }else{
            return response()->json([],401);
        }

        
    }
}
