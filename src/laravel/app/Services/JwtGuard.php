<?php

namespace App\Services;

use App\User;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;

class JwtGuard implements Guard
{
    use GuardHelpers;

    // private $provider;

    public function __construct(UserProvider $provider){
        $this->provider = $provider;
    }

    public function user(){

        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;
        $token = request()->bearerToken();

        try {
            
           $payload = (array) JWT::decode($token,'SECRETAITT',['HS256']);
           $user = User::where('email',$payload['email'])->firstOrFail();
            if (is_null($user)) {
                abort(401);
            }
        } catch (Exception $e) {
            
        }

        return $this->user = $user;

    }

    public function validate(array $credentials = []){
        // optional
    }

}

