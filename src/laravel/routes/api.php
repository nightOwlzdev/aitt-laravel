<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/users','CustomerController@index');

// Route::get('/users','CustomerController@index');

Route::apiResource('customers','Api\CustomerController')->middleware('auth:jwt');
Route::apiResource('users','Api\UserController');

Route::post('login','Api\AuthController@login');

// Route::apiResource('items','Api\ItemController');



